import os
from core.helper import clear_screen
from game.tetrisgame.tetris import main_menu
from game.pacmann.pacman import startpacmannn
# from game.pacmann.main import main
import game.pokemon as pokemm
# from game.pokete.pokete import mainn

def startgame():
    while True:

        clear_screen()
        print(r"""\
▄█─ ─ 　 ░█▀▀█ ─█▀▀█ ░█▀▀█ ░█▀▄▀█ ─█▀▀█ ░█▄─░█ 
─█─ ▄ 　 ░█▄▄█ ░█▄▄█ ░█─── ░█░█░█ ░█▄▄█ ░█░█░█ 
▄█▄ █ 　 ░█─── ░█─░█ ░█▄▄█ ░█──░█ ░█─░█ ░█──▀█""")
        print(r"""\
█▀█ ─ 　 ▀▀█▀▀ ░█▀▀▀ ▀▀█▀▀ ░█▀▀█ ▀█▀ ░█▀▀▀█ 
─▄▀ ▄ 　 ─░█── ░█▀▀▀ ─░█── ░█▄▄▀ ░█─ ─▀▀▀▄▄ 
█▄▄ █ 　 ─░█── ░█▄▄▄ ─░█── ░█─░█ ▄█▄ ░█▄▄▄█""")
        print(r"""\
█▀▀█ ─ 　 ░█▀▀█ ░█▀▀▀█ ░█─▄▀ ░█▀▀▀ ░█▀▄▀█ ░█▀▀▀█ ░█▄─░█ 
──▀▄ ▄ 　 ░█▄▄█ ░█──░█ ░█▀▄─ ░█▀▀▀ ░█░█░█ ░█──░█ ░█░█░█ 
█▄▄█ █ 　 ░█─── ░█▄▄▄█ ░█─░█ ░█▄▄▄ ░█──░█ ░█▄▄▄█ ░█──▀█""")

        choice = input(r"""\
▒█▀▀▀█ ▒█▀▀▀ ▒█░░░ ▒█▀▀▀ ▒█▀▀█ ▀▀█▀▀ ▄ 
░▀▀▀▄▄ ▒█▀▀▀ ▒█░░░ ▒█▀▀▀ ▒█░░░ ░▒█░░ ░ 
▒█▄▄▄█ ▒█▄▄▄ ▒█▄▄█ ▒█▄▄▄ ▒█▄▄█ ░▒█░░ ▀""")

        match choice:
            case '1':
                startpacmannn()
            case '2':
                main_menu()
            case '3':
                pokemm
            case 'other':
                pass

        # print('Login Success')
        # print('[1] Pacman')
        # print('[2] Tetris')
        # input('Pick ypur game: ')
        # is_exit = False
        # while input != 0:
        #     if input == '1':
        #         startpac()
            # if input == '2':
            #     tetrisstart()

            # return True

# def clear():
#     os.system('clear||cls')